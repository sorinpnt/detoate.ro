app = angular.module 'mainApp', [
  'ui.router'
  'app.header'
  'app.footer'
  'app.home'
]
app.config ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise 'home'
  $stateProvider
    .state 'home', {
      url: '/home'
      templateUrl: 'ui-views/home/home.html'
      controller: 'homeController'
    }