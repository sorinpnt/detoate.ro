header = angular.module 'app.header', [
  'header.controller'
  'header.resources'
  'header.directives'
]
