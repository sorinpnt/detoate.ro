/* File: gulpfile.js */
var gulp    = require('gulp');
var jade    = require('gulp-jade');
var less    = require('gulp-less');
var coffee  = require('gulp-coffee');
var concat  = require('gulp-concat');
var gutil   = require('gulp-util');
var express = require('express');
var tinylr  = require('tiny-lr');

//Handel COFEE files
gulp.task('compile_coffee', function() {
  return gulp.src('app/**/*.coffee')
   .pipe(coffee({ bare: true }))
   .pipe(concat('app.js'))
   .pipe(gulp.dest('./_public/includes/js/'));
});

// Handle LESS files
gulp.task('compile_less', function() {
  return gulp.src('app/*.less')
    .pipe(less())
    .pipe(gulp.dest('_public/includes/css/'));
});

// Handle JADE files
gulp.task('compile_index', function() {
  return gulp.src('index.jade')
    .pipe(jade())
    .pipe(gulp.dest('_public/'));
})
gulp.task('compile_jade', function() {
  return gulp.src('app/ui-views/*/*.jade')
    .pipe(jade())
    .pipe(gulp.dest('_public/ui-views/'));
});
// Handle RAW files
gulp.task('copy_raw', function() {
  return gulp.src(['./raw_data/**'])
        .pipe(gulp.dest('./_public/includes'));
});

// Start NODE server
gulp.task('start_server', function() {
  app = express();
  app.use(require('connect-livereload')({port: 8934}));
  app.use(express.static('./_public/'));
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.set('views', __dirname);

  app.get('/', function(req, res){
    res.render('_public/index.html');
  });
  app.listen(process.env.PORT || 4002);

});

gulp.task('livereload', function() {
  app = tinylr();
  app.listen(8934);
});

gulp.task('watch_dog', function() {
  gulp.watch('raw_data/**', ['copy_raw']);
  gulp.watch('app/**/*.coffee', ['compile_coffee']);
  gulp.watch('app/**/*.jade', ['compile_jade']);
  gulp.watch('app/**/*.less', ['compile_less']);
  gulp.watch('index.jade', ['compile_index']);
});

gulp.task('default', ['compile_less', 
                      'compile_index', 
                      'compile_jade', 
                      'copy_raw',
                      'compile_coffee',
                      'start_server',
                      'livereload',
                      'watch_dog']);